FROM node:12-alpine

RUN npm install -g typescript

# Set up the application directory
VOLUME ["/app","/app/experiment"]
ADD ./ /app/
WORKDIR /app

RUN tsc --p tsconfig.json
RUN pwd
RUN ls -ltr
RUN cp index.html app.js smoke.js ./experiment
RUN ls 
